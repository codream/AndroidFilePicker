package cn.luoxiaolong.android.picker.file.adapter

import android.support.v7.widget.RecyclerView
import cn.luoxiaolong.android.picker.file.bean.FileBean

abstract class BaseAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    abstract fun getItem(position: Int): FileBean?
}