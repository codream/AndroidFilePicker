package cn.luoxiaolong.android.picker.file.bean

import cn.luoxiaolong.android.picker.file.filetype.FileType

class FileItemBeanImpl(
    override var fileName: String,
    override var filePath: String,
    var fileTime: String,
    var fileSize: String,
    private var isChecked: Boolean,
    var fileType: FileType?,
    var isDir: Boolean,
    var isHide: Boolean,
    var beanSubscriber: BeanSubscriber
) : FileBean {

    fun isChecked(): Boolean {
        return isChecked
    }

    fun setCheck(check: Boolean) {
        isChecked = check
        beanSubscriber.updateItemUI(check)
    }

}