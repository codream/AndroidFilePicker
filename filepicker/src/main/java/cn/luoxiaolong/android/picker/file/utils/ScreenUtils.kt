package cn.luoxiaolong.android.picker.file.utils

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager

object ScreenUtils {
    /**
     * dp 转 px
     * @param context
     * @param dipValue
     * @return
     */
    fun dipToPx(context: Context, dipValue: Float): Int {
        val m = context.resources.displayMetrics.density
        return (dipValue * m + 0.5f).toInt()
    }


    /**
     * px 转 dp
     * @param context
     * @param px
     * @return
     */
    fun pxToDip(context: Context?, px: Float): Float {
        return if (context == null) {
            -1f
        } else px / context.resources.displayMetrics.density
    }


    /**
     * sp 转 px
     * @param context
     * @param spValue
     * @return
     */
    fun spToPx(context: Context, spValue: Float): Int {
        val fontScale = context.resources.displayMetrics.scaledDensity
        return (spValue * fontScale + 0.5f).toInt()
    }


    /**
     * 获得屏幕宽度像素
     * @param context
     * @return
     */
    fun getScreenWidthInPixel(context: Context): Int {
        var wm = context
            .getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val outMetrics = DisplayMetrics()
        wm.defaultDisplay.getMetrics(outMetrics)
        val screenW = outMetrics.widthPixels
        return screenW
    }

    /**
     * 获得屏幕高度像素
     * @param context
     * @return
     */
    fun getScreenHeightInPixel(context: Context): Int {
        var wm = context
            .getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val outMetrics = DisplayMetrics()
        wm.defaultDisplay.getMetrics(outMetrics)
        val screenH = outMetrics.heightPixels
        return screenH
    }

    /**
     * 是否横屏
     */
    fun isLandScape(context: Context): Boolean {
        return getScreenWidthInPixel(context) > getScreenHeightInPixel(context)
    }

    /**
     * 将像素转为 dp
     * @param context
     * @param pxValue 像素
     * @return
     */
    fun px2dp(context: Context, pxValue: Float): Int {
        return (pxValue / context.resources.displayMetrics.density).toInt()
    }

}
