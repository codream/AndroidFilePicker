package cn.luoxiaolong.android.picker.file.bean

/**
 *
 * @author long
 * @date 2018/11/21
 */
interface FileBean {
    var fileName: String
    var filePath: String
}