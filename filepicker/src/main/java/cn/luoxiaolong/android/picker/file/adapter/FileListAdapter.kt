package cn.luoxiaolong.android.picker.file.adapter

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView

import cn.luoxiaolong.android.picker.file.FilePickerActivity
import cn.luoxiaolong.android.picker.file.R
import cn.luoxiaolong.android.picker.file.bean.FileItemBeanImpl
import cn.luoxiaolong.android.picker.file.config.FilePickerManager

import java.io.File

class FileListAdapter(private val activity: FilePickerActivity, var data: ArrayList<FileItemBeanImpl>?) :
    BaseAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(activity).inflate(R.layout.item_list_file_picker, parent, false)
        return FileListItemHolder(itemView)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 10
    }

    override fun getItemViewType(position: Int): Int {
        return DEFAULT_FILE_TYPE
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as FileListItemHolder).bind(data!![position], position)
    }

    override fun getItem(position: Int): FileItemBeanImpl? {
        if (position >= 0 &&
            position < data!!.size &&
            getItemViewType(position) == DEFAULT_FILE_TYPE
        ) return data!![position]
        return null
    }

    inner class FileListItemHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        private val isSkipDir: Boolean = FilePickerManager.config?.isSkipDir ?: true
        private val isMultiple: Boolean = FilePickerManager.config?.isMultiple ?: false

        private val mTvFileName = itemView.findViewById<TextView>(R.id.tv_list_file_picker)!!
        private val mTvFileTime = itemView.findViewById<TextView>(R.id.tv_list_file_time)!!
        private val mTvFileSize = itemView.findViewById<TextView>(R.id.tv_list_file_size)!!
        private val mCbItem = itemView.findViewById<CheckBox>(R.id.cb_list_file_picker)!!
        private val mIcon = itemView.findViewById<ImageView>(R.id.iv_icon_list_file_picker)!!
        private var mItemBeanImpl: FileItemBeanImpl? = null
        private var mPosition: Int? = null

        fun bind(itemImpl: FileItemBeanImpl, position: Int) {
                       mItemBeanImpl = itemImpl
            mPosition = position

            mTvFileName.text = itemImpl.fileName
            mTvFileTime.text = itemImpl.fileTime

            mTvFileSize.text = itemImpl.fileSize
            if (TextUtils.isEmpty(itemImpl.fileSize)) {
                mTvFileSize.visibility = View.GONE
            } else {
                mTvFileSize.visibility = View.VISIBLE
            }

            mCbItem.isChecked = itemImpl.isChecked()
            val isDir = File(itemImpl.filePath).isDirectory
            if (isDir) {
                mIcon.setImageResource(R.drawable.ic_folder_file_picker)
            } else {
                val resId: Int = itemImpl.fileType?.fileIconResId ?: R.drawable.ic_unknown_file_picker
                mIcon.setImageResource(resId)
            }
            if (isMultiple) {
                if (isDir && isSkipDir) {
                    mCbItem.visibility = View.GONE
                    return
                }
                mCbItem.visibility = View.VISIBLE
            } else {
                mCbItem.visibility = View.GONE
            }
        }
    }

    companion object {
        const val DEFAULT_FILE_TYPE = 10001
    }
}